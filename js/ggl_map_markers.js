(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.gglMapMarkers = Drupal.gglMapMarkers || {};

  // Set on loading the collections.
  Drupal.gglMapMarkers.allMarkers = Drupal.gglMapMarkers.allMarkers || {};

  // Set on clustering the markers.
  Drupal.gglMapMarkers.markerClusters = Drupal.gglMapMarkers.markerClusters || {};

  // Set on marker click.
  Drupal.gglMapMarkers.lastOpenMarker = Drupal.gglMapMarkers.lastOpenMarker || undefined;

  /**
   * Initialize the markers based on the data coming from the collection.
   * @param collection
   * @param collectionData
   */
  Drupal.gglMapMarkers.initMarkers = function (collection, collectionData) {
    let _self = this;

    if ("items" in collectionData) {
      $.each(collectionData.items, function (index, markerData) {
        if ("id" in markerData) {
          if (!(markerData.id in _self.allMarkers)) {
            _self.allMarkers[markerData.id] = Drupal.gglMapMarkers.createMarker(collection, markerData);
          }
          else {
            let marker = _self.allMarkers[markerData.id];
            _self.updateMarker(marker, markerData);
            marker.setMap(Drupal.gglMap.map);
          }

          // Add collection name to marker data.
          _self.addCollectionToMarker(collection.id, _self.allMarkers[markerData.id]);
        }
      });
    }
  };

  /**
   * Update the marker based on the data coming from the collection endpoint.
   * @param marker
   * @param markerData
   */
  Drupal.gglMapMarkers.updateMarker = function (marker, markerData) {
    let _self = this;

    let firstCollection = Drupal.gglMapCollections.collections[marker.data.collection[0]];
    let prepareMarker = _self.prepareMarker(firstCollection, markerData);
    marker.setPosition(prepareMarker.position);
    marker.setIcon(prepareMarker.icon);
    marker.infoWindow.content = prepareMarker.domPopupHtml;

    let origMarkerDataCollection = marker.data.collection;
    marker.data = markerData;
    marker.data.collection = origMarkerDataCollection;
  };

  /**
   * Create the marker based on the data coming from the collection endpoint.
   * @param collection
   * @param markerData
   * @returns {google.maps.Marker}
   */
  Drupal.gglMapMarkers.createMarker = function (collection, markerData) {
    let _self = this;
    let prepareMarker = _self.prepareMarker(collection, markerData);

    /* Create marker object itself. */
    let marker = new google.maps.Marker({
      data: markerData,
      map: Drupal.gglMap.map,
      position: prepareMarker.position,
      icon: prepareMarker.icon,
      visible: true,
      infoWindow: new google.maps.InfoWindow({
        content: prepareMarker.domPopupHtml,
        maxWidth: 285
      })
    });

    /* Bind click event to marker object. */
    google.maps.event.addListener(marker, "click", function () {
      _self.openPopup(marker);

      // Trigger external JS after opening popup..
      $(window).trigger('gglMap:onClickMarker', marker);
    });

    /* Add the markr to the map. */
    marker.setMap(Drupal.gglMap.map);

    return marker;
  };

  /**
   * Prepare marker data for creating or updating a marker.
   */
  Drupal.gglMapMarkers.prepareMarker = function (forCollection, markerData) {
    let _self = this;

    // Determine latitude & longitude from data. */
    let position = _self.getMarkerPosition(markerData);

    /* Create icon object. */
    let icon = _self.getMarkerIcon(forCollection, markerData);

    /* Prepare a div element for popup. */
    let domPopupHtml = document.createElement('div');

    /* Prepare popup html. */
    let content = markerData.popup.content;
    if (content === "" || typeof content === 'undefined') {
      content = "<span id='vl_popup_loading' class='" + drupalSettings.gglMap.marker.popupLoadingClass + "'></span>";
    }

    /* Set the popup html. */
    domPopupHtml.innerHTML = '<div id="popup_content_' + markerData.id + '">' + content + '</div>';

    return {
      position: position,
      icon: icon,
      domPopupHtml: domPopupHtml
    };
  };

  /**
   * Get all visible markers.
   * @returns {*[]}
   */
  Drupal.gglMapMarkers.visibleMarkers = function (forCollection = 'all') {
    let visibleMarkers = [];
    let markerIds = Object.keys(Drupal.gglMapMarkers.allMarkers);
    markerIds.forEach((markerId) => {
      let marker = Drupal.gglMapMarkers.allMarkers[markerId];
      if (forCollection !== 'all') {
        if (!marker.data.collection.includes(forCollection)) {
          return; // next marker
        }
      }
      if (marker.visible === true) {
        visibleMarkers.push(marker);
      }
    });
    return visibleMarkers;
  };

  /**
   * Add collection to marker.
   * @param collectionId
   * @param marker
   */
  Drupal.gglMapMarkers.addCollectionToMarker = function (collectionId, marker) {
    if (!('collection' in marker.data)) {
      marker.data.collection = [];
    }
    if (!marker.data.collection.includes(collectionId)) {
      marker.data.collection.push(collectionId);
    }
  };

  /**
   * Remove collection from marker.
   * @param collectionId
   * @param marker
   */
  Drupal.gglMapMarkers.removeCollectionFromMarker = function (collectionId, marker) {
    let _self = this;
    if (typeof marker !== 'undefined') {
      let position = marker.data.collection.indexOf(collectionId);
      if (position > -1) {
        marker.data.collection.splice(position, 1);
      }

      // If no collections left, remove the marker.
      if (marker.data.collection.length === 0) {
        marker.setVisible(false);
        marker.setMap(null);
        delete _self.allMarkers[marker.data.id];
        Drupal.gglMap.refresh();
      }
    }
  };

  /**
   * Get all visible markers.
   * @returns {*[]}
   */
  Drupal.gglMapMarkers.collectionMarkers = function (forCollection) {
    let collectionMarkers = [];
    let markerIds = Object.keys(Drupal.gglMapMarkers.allMarkers);
    markerIds.forEach((markerId) => {
      let marker = Drupal.gglMapMarkers.allMarkers[markerId];
      if (marker.data.collection.includes(forCollection)) {
        collectionMarkers.push(marker);
      }
    });
    return collectionMarkers;
  };

  /**
   * Initialize marker clusters.
   */
  Drupal.gglMapMarkers.initMarkerClusters = function () {
    let _self = this;
    if (Drupal.gglMap.isEmpty(_self.markerClusters)) {
      _self.markerClusters = new MarkerClusterer(Drupal.gglMap.map, new Array(), drupalSettings.gglMap.cluster);
    }

    let visibleMarkers = _self.visibleMarkers();
    this.markerClusters.addMarkers(visibleMarkers);
    Drupal.gglMap.refresh();
  };

  Drupal.gglMapMarkers.getPopupContentViaUrl = function (url, callback) {
    $.ajax({
      url: url,
      dataType: "json",
      async: true,
    }).fail(function () {
      // Proceed to fail callback.
      if (typeof callback !== 'undefined') { callback(); }
    }).always(function (content, textStatus) {
      if (textStatus === "success") {
        // Proceed to success callback.
        if (typeof callback !== 'undefined') { callback(content); }
      }
    });
  };

  Drupal.gglMapMarkers.openPopup = function (marker) {
    let _self = this;

    // Close the open popup if one.
    _self.closeOpenPopup();

    // Move clicked marker to the center.
    if (drupalSettings.gglMap.marker.onOpenPopup.centerToMarker === true) {
      _self.centerToMarker(marker);
    }

    // Open current infoWindow of current marker.
    let infoWindow = marker.infoWindow;
    infoWindow.open(Drupal.gglMap.map, marker);
    // popupContent not loaded via collection then do it via popup endpoint.
    if (typeof marker.data.popup.content === 'undefined') {
      _self.updatePopupContentViaUrl(marker.data, function () {
        // Store current marker as last one.
        _self.lastOpenMarker = marker;

        // Trigger external JS after opening popup.
        $(window).trigger('gglMap:afterOpenPopup',  marker);
      });
    }
    else {
      // #popup_content_[id] only exists after infoWindow.open so we update the HTML here.
      $("#popup_content_" + marker.data.id).html(marker.data.popup.content);

      // Store current marker as last one.
      _self.lastOpenMarker = marker;

      // Trigger external JS after opening popup.
      $(window).trigger('gglMap:afterOpenPopup', marker);
    }


  };

  /**
   * Close the popup.
   * @param marker
   */
  Drupal.gglMapMarkers.closePopup = function (marker) {
    let _self = this;
    marker.infoWindow.close();
    if ('data' in marker && typeof _self.lastOpenMarker !== 'undefined' && marker.data.id === _self.lastOpenMarker.data.id) {
      _self.lastOpenMarker = undefined;
    }

    // Trigger external JS after close popup.
    $(window).trigger('gglMap:afterClosePopup', marker);
  };

  /**
   * Close the current open popup.
   */
  Drupal.gglMapMarkers.closeOpenPopup = function () {
    let _self = this;
    if (typeof _self.lastOpenMarker !== 'undefined') {
      _self.closePopup(_self.lastOpenMarker);
    }
  };

  /**
   * Pan to specific marker.
   *
   * @param marker
   * @param zoomLevel
   * @param openPopup
   */
  Drupal.gglMapMarkers.centerToMarker = function (marker) {
    // Initiate zoomLevel to current zoom level.
    let zoomLevel = Drupal.gglMap.map.getZoom();
    if (drupalSettings.gglMap.marker.onOpenPopup.zoomToMarker === true) {
      zoomLevel = drupalSettings.gglMap.marker.onOpenPopup.zoomLevel;
    }

    // In case the marker would be invisible.
    marker.setVisible(true);

    Drupal.gglMap.moveTo(marker.position, zoomLevel);
  };

  /**
   * Update marker icon.
   *
   * @param markerId
   * @param icon
   */
  Drupal.gglMapMarkers.updateMarkerIcon = function (markerId, icon) {
    if (markerId in Drupal.gglMapMarkers.allMarkers) {
      let updatedIcon = {
        size: new google.maps.Size(icon.height, icon.width),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(icon.height / 2, icon.width / 2),
        scaledSize: new google.maps.Size(icon.height, icon.width),
        url: icon.url
      };

      Drupal.gglMapMarkers.allMarkers[markerId].setIcon(updatedIcon);
    }
  };

  /**
   * Update popup html.
   *
   * @param markerData
   * @param callback
   */
  Drupal.gglMapMarkers.updatePopupContentViaUrl = function (markerData, callback) {
    let _self = this;
    let markerId = markerData.id;
    if (markerId in _self.allMarkers) {
      _self.getPopupContentViaUrl(markerData.popup.url, function (content) {
        $("#popup_content_" + markerId).html(content);

        let marker = _self.allMarkers[markerId];
        marker.data.popup.content = content;

        // Trigger external JS after opening popup..
        $(window).trigger('gglMap:afterUpdatePopup', marker);

        if (typeof callback !== 'undefined') { callback(); }
      });
    }
  };

  /**
   * Click marker.
   *
   * @param markerId
   */
  Drupal.gglMapMarkers.clickMarker = function (markerId) {
    let marker = Drupal.gglMapMarkers.allMarkers[markerId];
    google.maps.event.trigger(marker, "click", {});
  }

  /**
   * Get marker position object.
   *
   * @param markerData
   * @returns {google.maps.LatLng}
   */
  Drupal.gglMapMarkers.getMarkerPosition = function(markerData) {
    let lat;
    let lng;
    if (Array.isArray(markerData.lat)) { lat = markerData.lat[0]; } else { lat = markerData.lat; }
    if (Array.isArray(markerData.lng)) { lng = markerData.lng[0]; } else { lng = markerData.lng; }

    /* Create position object from latitude and longitude. */
    return new google.maps.LatLng({
      lat: parseFloat(lat),
      lng: parseFloat(lng)
    });
  }

  /**
   * Get marker icon object.
   *
   * @param forCollection
   * @param markerData
   * @returns {{scaledSize: google.maps.Size, size: google.maps.Size, origin: google.maps.Point, anchor: google.maps.Point}}
   */
  Drupal.gglMapMarkers.getMarkerIcon = function(forCollection, markerData) {
    let iconData = {};

    /* Determine the icon url. */
    if (typeof markerData.icon !== "undefined" && typeof markerData.icon.url !== "undefined") {
      iconData.url = markerData.icon.url;
    }
    else {
      // If not check use the icon data from the collection.
      if (typeof forCollection.icon !== "undefined" && typeof forCollection.icon.url !== "undefined") {
        iconData.url = forCollection.icon.url;
      }
      else {
        // If not use the default setting.
        iconData.url = drupalSettings.gglMap.marker.icon.url;
      }
    }

    /* Determine the icon height. */
    if (typeof markerData.icon !== "undefined" && typeof markerData.icon.height !== "undefined") {
      iconData.height = markerData.icon.height;
    }
    else {
      // If not check use the icon data from the collection.
      if (typeof forCollection.icon !== "undefined" && typeof forCollection.icon.height !== "undefined") {
        iconData.height = forCollection.icon.height;
      }
      else {
        // If not use the default setting.
        iconData.height = drupalSettings.gglMap.marker.icon.height;
      }
    }

    /* Determine the icon width. */
    if (typeof markerData.icon !== "undefined" && typeof markerData.icon.width !== "undefined") {
      iconData.width = markerData.icon.width;
    }
    else {
      // If not check use the icon data from the collection.
      if (typeof forCollection.icon !== "undefined" && typeof forCollection.icon.width !== "undefined") {
        iconData.width = forCollection.icon.width;
      }
      else {
        // If not use the default setting.
        iconData.width = drupalSettings.gglMap.marker.icon.width;
      }
    }

    return {
      size: new google.maps.Size(iconData.height, iconData.width),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(iconData.height / 2, iconData.width / 2),
      scaledSize: new google.maps.Size(iconData.height, iconData.width),
      url: iconData.url
    };
  }

})(jQuery, Drupal, drupalSettings);
