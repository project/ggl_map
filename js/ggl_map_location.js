(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.gglMapLocation = Drupal.gglMapLocation || {};
  Drupal.gglMapLocation.location = Drupal.gglMapLocation.location || {};
  Drupal.gglMapLocation.locationMarker = Drupal.gglMapLocation.locationMarker || {};

  /**
   *
   */
  Drupal.gglMapLocation.initLocationSearch = function () {
    let _self = this;
    let input = document.getElementById("ggl_map_search_location_text");
    let autocomplete = new google.maps.places.Autocomplete(input, {
      types: ['(cities)']
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      let place = autocomplete.getPlace();
      _self.location = {
        'lat': parseFloat(place.geometry.location.lat()),
        'lng': parseFloat(place.geometry.location.lng())
      };
      Drupal.gglMap.moveTo(_self.location, 13);

      _self.showLocationMarker();
    });

    $("#ggl_map_search_location_button").once().on('click', function () {

      let place = autocomplete.getPlace();
      _self.location = {
        'lat': parseFloat(place.geometry.location.lat()),
        'lng': parseFloat(place.geometry.location.lng())
      };

      Drupal.gglMap.moveTo(_self.location, 13);

      _self.showLocationMarker();
    });
  };

  /**
   *
   */
  Drupal.gglMapLocation.initCurrentLocation = function () {
    let _self = this;

    $("#ggl_map_current_location_button").once().on('click', function () {
      $('#ggl_map_current_location_error').css('display', 'none');
      _self.goToCurrentLocation(function (success) {
        if (success === false) {
          const messages = new Drupal.Message();
          messages.add(
            Drupal.t('Something went wrong. Check your browser settings.'), { type: 'error' });
        }
      });
    });
  };

  /**
   *
   */
  Drupal.gglMapLocation.goToCurrentLocation = function (callback) {
    let _self = this;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
          _self.location = {
            'lat': position.coords.latitude,
            'lng': position.coords.longitude
          };

          Drupal.gglMap.moveTo(_self.location, 13);

          // Show the current location marker is one is foreseen.
          if (Drupal.gglMapLocation.locationMarker !== '') {
            _self.showLocationMarker();
          }

          if (typeof callback !== 'undefined') {
            callback(true);
          }
        },
        function (error) {
          switch (error.code) {
            case error.PERMISSION_DENIED:
              console.log("User denied the request for Geolocation.");
              break;

            case error.POSITION_UNAVAILABLE:
              console.log("Location information is unavailable.");
              break;

            case error.TIMEOUT:
              console.log("The request to get user location timed out.");
              break;

            case error.UNKNOWN_ERROR:
              console.log("An unknown error occurred.");
              break;
          }
          if (typeof callback !== 'undefined') {
            callback(false);
          }
        });
    }
  };

  /**
   *
   */
  Drupal.gglMapLocation.showLocationMarker = function () {
    let _self = this;

    if (Drupal.gglMap.isEmpty(_self.locationMarker)) {
      _self.locationMarker = new google.maps.Marker({
        map: Drupal.gglMap.map,
        position: new google.maps.LatLng({
          lat: parseFloat(_self.location.lat),
          lng: parseFloat(_self.location.lng)
        }),
        icon: {
          size: new google.maps.Size(drupalSettings.gglMap.location.currentLocationIcon.height, drupalSettings.gglMap.location.currentLocationIcon.width),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(drupalSettings.gglMap.location.currentLocationIcon.height / 2, drupalSettings.gglMap.location.currentLocationIcon.width / 2),
          scaledSize: new google.maps.Size(drupalSettings.gglMap.location.currentLocationIcon.height, drupalSettings.gglMap.location.currentLocationIcon.width),
          url: drupalSettings.gglMap.location.currentLocationIcon.url
        },
        visible: true,
      });

      _self.locationMarker.setMap(Drupal.gglMap.map);
    }
    else {
      _self.locationMarker.setPosition(new google.maps.LatLng({
        lat: _self.location.lat,
        lng: _self.location.lng
      }));
    }
  };

})(jQuery, Drupal, drupalSettings);
