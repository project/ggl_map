(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.gglMapToggles = Drupal.gglMapToggles || {};

  /**
   * Toggle all collection markers.
   * @param forCollection
   * @param keepCollectionVisible
   * @param state
   * @param fitMapToMarkers
   */
  Drupal.gglMapToggles.toggleCollection = function (forCollection, state, keepCollectionVisible = '', fitMapToMarkers = true) {

    let collection = Drupal.gglMapCollections.collections[forCollection];
    collection.visible = state;

    Drupal.gglMapCollections.loadCollection(
      collection,
      // callback
      function (collection, collectionData) {
        Drupal.gglMapCollections.processCollection(collection, collectionData);

        // Toggle collection markers depending on state.
        Drupal.gglMapMarkers.collectionMarkers(forCollection).forEach((marker) => {
          if (!marker.data.collection.includes(keepCollectionVisible)) {
            marker.setVisible(state);
            // Close popup if marker is set invisble & is opened.
            if (state === false && marker === Drupal.gglMapMarkers.lastOpenMarker) {
              Drupal.gglMapMarkers.closePopup(Drupal.gglMapMarkers.lastOpenMarker);
            }
          }
        });

        // (re)initiaze the cluster.
        if (drupalSettings.gglMap.cluster.enableClustering === true) {
          Drupal.gglMapMarkers.initMarkerClusters();
        }

        // Fit map to markers if needed.
        if (fitMapToMarkers && drupalSettings.gglMap.map.fitMapToMarkers === true) {
          let visibleMarkers = Drupal.gglMapMarkers.visibleMarkers();
          Drupal.gglMap.fitMapToMarkers(visibleMarkers);
        } else {
          Drupal.gglMap.refresh();
        }
      });
  };

})(jQuery, Drupal, drupalSettings);
