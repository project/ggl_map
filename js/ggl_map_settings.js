(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.gglMapSettings = Drupal.gglMapSettings || {};

  /**
   * Replace Google Constants by there correct value.
   */
  Drupal.gglMapSettings.replaceGoogleConstants = function () {
    drupalSettings.gglMap.map.mapTypeControlOptions.mapTypeIds = eval(drupalSettings.gglMap.map.mapTypeControlOptions.mapTypeIds);
    drupalSettings.gglMap.map.mapTypeControlOptions.position = eval(drupalSettings.gglMap.map.mapTypeControlOptions.position);
    drupalSettings.gglMap.map.mapTypeControlOptions.style = eval(drupalSettings.gglMap.map.mapTypeControlOptions.style);
    drupalSettings.gglMap.map.zoomControlOptions.position = eval(drupalSettings.gglMap.map.zoomControlOptions.position);
  }

})(jQuery, Drupal, drupalSettings);
