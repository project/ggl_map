(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.gglMapFilters = Drupal.gglMapFilters || {};
  Drupal.gglMapFilters.filterProperties = Drupal.filterProperties || {};

  /**
   * Apply the filters on all markers or on the markers of a specific collection.
   * @param forCollection
   * @param fitMapToMarkers
   */
  Drupal.gglMapFilters.applyFilters = function (forCollection = 'all', fitMapToMarkers = true) {
    let _self = this;

    // Collect filter data.
    _self.initFilterProperties(forCollection);

    // Loop over all actFilters by the property key.
    let properties = Object.keys(Drupal.gglMapFilters.filterProperties);
    properties.forEach((property) => {
      // The filter to apply.
      let filter = Drupal.gglMapFilters.filterProperties[property];

      // If only a specific collection is filters and the filter is about another collection, go to next filter.
      if (forCollection !== 'all' && forCollection !== filter.collection) {
        return;
      }

      // Match every loaded marker against the filter.
      let markerIds = Object.keys(Drupal.gglMapMarkers.allMarkers);
      markerIds.forEach((markerId) => {
      //Drupal.gglMapMarkers.allMarkers.forEach((marker) => {
        let marker = Drupal.gglMapMarkers.allMarkers[markerId];
        marker.data.collection.forEach((markerCollection) => {
          if (markerCollection !== filter.collection) {
            return;
          }
          // Evaluate filter value for marker.
          if (_self.evaluateFilterValueForMarker(property, filter.value, filter.operator, marker.data.forFilter)) {
            // If marker matches the filter make it visible.
            marker.setVisible(true);
          } else {
            // If marker does not match the filter make it invisible.
            marker.setVisible(false);
          }
        });
      });
    });

    // (re)initiaze the cluster.
    if (drupalSettings.gglMap.cluster.enableClustering === true) {
      Drupal.gglMapMarkers.initMarkerClusters();
    }

    // Fit map to markers if needed.
    if (fitMapToMarkers && drupalSettings.gglMap.map.fitMapToMarkers === true) {
      let visibleMarkers = Drupal.gglMapMarkers.visibleMarkers();
      Drupal.gglMap.fitMapToMarkers(visibleMarkers);
    }
    else {
      Drupal.gglMap.refresh();
    }
  };

  /**
   * Initialize the active filters for all collections or those of a specific collection.
   * @param forCollection
   */
  Drupal.gglMapFilters.initFilterProperties = function (forCollection = 'all') {
    // Initialize the filterProperties variable.
    Drupal.gglMapFilters.filterProperties = {};

    // All elements with the attribute 'data-ggl-map-filter-property' are taken into account as map filters.
    $("[data-ggl-map-filter-property]").each( function() {

      // Only filters with the attribute active filters are taken into account.
      if ($(this).attr('data-ggl-map-filter-active') === 'true') {
        // Collect active filter data.
        let property = $(this).attr('data-ggl-map-filter-property');
        let value = $(this).attr('data-ggl-map-filter-value');
        let collection = $(this).attr('data-ggl-map-filter-collection');
        let operator = (typeof $(this).attr('data-ggl-map-filter-operator') !== 'undefined') ? $(this).attr('data-ggl-map-filter-operator') : 'and';

        // If only a specific collection is filtered and the filter collection does not match it, go to the next filter.
        if (forCollection !== 'all') {
          if (forCollection !== collection) {
            return;
          }
        }

        // Property already defined as active filter? Add value, else add filter.
        if (typeof Drupal.gglMapFilters.filterProperties[property] !== "undefined") {
          Drupal.gglMapFilters.filterProperties[property].value.push(value);
        } else {
          // Add it to the filters object.
          Drupal.gglMapFilters.filterProperties[property] = {
            'value': [value],
            'collection': collection,
            'operator': operator
          };
        }
      }
    });
  };

  /**
   * Evaluate the filter value(s) for a specific marker.
   *
   * @param filterProperty
   * @param filterValue
   * @param filterOperator
   * @param markerForFilterValues
   * @returns {boolean}
   */
  Drupal.gglMapFilters.evaluateFilterValueForMarker = function (filterProperty, filterValue, filterOperator, markerForFilterValues) {
    // Initialize the a match counter.
    let matchCount = 0;

    // If the value is 'on' or 'off' the filter is a toggle filter and returns true or false immediately.
    if (filterValue === ["on"]) {
      return true;
    }
    if (filterValue === ["off"]) {
      return false;
    }

    // If filter has one ore more values, match them all against the markerForFilterValues.
    filterValue.forEach((value) => {
      // Another value means it comes for one or more values filter.
      if ((typeof markerForFilterValues[filterProperty] !== "undefined" && markerForFilterValues[filterProperty].includes(value) || value === "on")) {
        // If operator of the filter is 'or' then one match is enough to return 'true'.
        if (filterOperator === "or") {
          return true;
        }
        // For the 'and' operator count the matches.
        matchCount++;
      }
    });

    // If operator of the filter is 'and' all values must match the markerForFilter to return 'true'.
    if (filterOperator === "and") {
      if (matchCount === filterValue.length) {
        return true;
      }
    }

    // In any other case return 'false'.
    return false;
  };

})(jQuery, Drupal, drupalSettings);
