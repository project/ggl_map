(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.gglMapCollections = Drupal.gglMapCollections || {};
  Drupal.gglMapCollections.collections = Drupal.gglMapCollections.collections || {};
  Drupal.gglMapCollections.loadingCounter = Drupal.gglMapCollections.loadingCounter || 0;

  Drupal.gglMapCollections.loadCollections = function (viaBoundsChange = false, allCollectionsLoadedCallback) {
    let apiPromises = [];
    let _self = this;
    let collectionsToLoad;

    if (Drupal.gglMap.isEmpty(Drupal.gglMapCollections.collections)) {
      collectionsToLoad = drupalSettings.gglMap.collection;
    }
    else {
      collectionsToLoad = Drupal.gglMap.objectToArray(Drupal.gglMapCollections.collections);
    }

    if (typeof collectionsToLoad !== "undefined" && collectionsToLoad !== null) {
      // Trigger external JS when loading collection starts.
      $(window).trigger('gglMap:startLoadingCollections', collectionsToLoad);

      Drupal.gglMap.collectionsLoading = true;
      collectionsToLoad.forEach((collection) => {
        if (viaBoundsChange === true) {
          if (collection.limitToBounds === false) {
            return;
          }
        }

        if ('items' in collection) {
          _self.processCollection(collection, {'items': collection.items});
        }
        else {
          // Repeat the below for each ajax request
          apiPromises.push(
            new Promise(function (resolve, reject) {
              _self.loadCollection(
                collection,
                // callback
                function (collection, collectionData) {
                  _self.processCollection(collection, collectionData);
                  /*
                  if (!(collection.id in _self.collections)) {
                    _self.collections[collection.id] = collection;
                  }
                   */
                  resolve();
                });
            }));
        }
      });

      // When all collections are loaded.
      Promise.all(apiPromises).then(() => {
        Drupal.gglMap.collectionsLoading = false;

        // all requests finished successfully.
        if (typeof allCollectionsLoadedCallback !== 'undefined') {
          allCollectionsLoadedCallback(true);
        }

        // Trigger external JS after loading collections.
        $(window).trigger('gglMap:afterLoadingCollections', {'result': true});
      }).catch(() => {
        Drupal.gglMap.collectionsLoading = false;

        // all requests finished but one or more failed.
        if (typeof allCollectionsLoadedCallback !== 'undefined') {
          allCollectionsLoadedCallback(false);
        }

        // Trigger external JS after loading collections.
        $(window).trigger('gglMap:afterLoadingCollections', {'result': false});
      });
    }
    else {
      if (drupalSettings.gglMap.project.debugging === true) {
        console.log('No collections to load');
      }
    }
  };

  Drupal.gglMapCollections.loadCollection = function (collection, callback) {
    let _self = this;

    _self.startLoadingCollection(collection);

    if (!('visible' in collection)) {
      collection.visible = collection.toggleOnLoad;
    }

    if (collection.visible === false) {
      if (typeof callback !== 'undefined') {
        callback(collection, []);
      }
      _self.afterLoadingCollection(collection, false);
      return;
    }

    let url = _self.getCollectionUrl(collection);
    if (url !== '') {
      $.ajax({
        url: url,
        dataType: "json",
        async: true,
      }).fail(function () {
        // Sebugging.
        if (drupalSettings.gglMap.project.debugging === true) {
          console.log("Loading '" + collection.id + "' data failed.");
          console.log(url);
        }

        // Proceed to fail callback.
        if (typeof callback !== 'undefined') {
          callback(collection, [null]);
        }
        _self.afterLoadingCollection(collection, false);
      }).always(function (collectionData, textStatus) {
        if (textStatus === "success") {
          // Sebugging.
          if (drupalSettings.gglMap.project.debugging === true) {
            console.log("Loading '" + collection.id + "' data successfully.");
            console.log(url);
          }

          // Proceed to success callback.
          if (typeof callback !== 'undefined') {
            callback(collection, collectionData);
          }
          _self.afterLoadingCollection(collection, true);
        }
      });
    }
    else {
      if (typeof callback !== 'undefined') {
        callback(collection, []);
      }
      _self.afterLoadingCollection(collection, false);
    }
  };

  /**
   * Process the collection.
   *
   * @param collection
   * @param collectionData
   */
  Drupal.gglMapCollections.processCollection = function (collection, collectionData) {
    let _self = this;
    if (collectionData[0] === null) {
      if (drupalSettings.gglMap.project.debugging === true) {
        console.log("An error occured for collection '" + collection.id + "'.");
      }

      // Trigger external JS to handle the collection loading failure.
      $(window).trigger('gglMap:errorLoadingCollection', collection);
    }
    else {
      // Initialize the markers.
      Drupal.gglMapMarkers.initMarkers(collection, collectionData);

      // Cluster the markers.
      if (drupalSettings.gglMap.cluster.enableClustering === true) {
        Drupal.gglMapMarkers.initMarkerClusters();
      }

      if (!(collection.id in _self.collections)) {
        _self.collections[collection.id] = collection;
      }
    }
  };

  /**
   * Reload the collection with a specific name.
   *
   * @param name
   */
  Drupal.gglMapCollections.reloadCollectionByName = function (name) {
    let _self = this;
    if (name in _self.collections) {
      let collection = _self.collections[name];
      _self.loadCollection(
        collection,
        // callback
        function (collection, collectionData) {
          _self.processCollection(collection, collectionData);
        }
      );
    }
  };

  /**
   * Get the collection URL extended with boundary parameters if limited to
   * bounds.
   *
   * @param collection
   * @returns {string}
   */
  Drupal.gglMapCollections.getCollectionUrl = function (collection) {
    let collectionUrl = collection.url;
    if (collection.limitToBounds === true) {
      let mapBounds = Drupal.gglMap.getMapBounds();

      // The same neLat & swLat and neLng & swLng means propably that the map is not visible (yet). So skip this collection.
      if (mapBounds.neLat === mapBounds.swLat && mapBounds.neLng === mapBounds.swLng) {
        return "";
      }
      collectionUrl = collectionUrl + "/" + mapBounds.swLat + '--' + mapBounds.neLat + '/' +  mapBounds.swLng + '--' + mapBounds.neLng;
    }

    if (!Drupal.gglMap.isEmpty(collection.queryParameters)) {
      let queryString = '';
      let keys = Object.keys(collection.queryParameters);
      keys.forEach((key) => {
        if (queryString !== '') { queryString += '&'; }
        queryString += key + "=" + collection.queryParameters[key];
      });
      collectionUrl += '?' + queryString;
    }

    return collectionUrl;
  };

  /**
   * Start loading collection.
   */
  Drupal.gglMapCollections.startLoadingCollection = function (collection) {
    let _self = this;
    _self.loadingCounter++;

    // Trigger external JS when loading a collection starts.
    $(window).trigger('gglMap:startLoadingCollection', {
      'collection' : collection,
      'loadingCounter': _self.loadingCounter,
    });
  };

  /**
   * After loading collection.
   */
  Drupal.gglMapCollections.afterLoadingCollection = function (collection, result) {
    let _self = this;
    _self.loadingCounter--;

    // Trigger external JS when loading a collection ands.
    $(window).trigger('gglMap:afterLoadingCollection', {
      'collection': collection,
      'result': result,
      'loadingCounter': _self.loadingCounter,
    });
  };

})(jQuery, Drupal, drupalSettings);
