(function($, Drupal) {

  Drupal.AjaxCommands.prototype.clickMarker = function(ajax, response, status){
    let markerId = response.markerId;

    Drupal.gglMapMarkers.clickMarker(markerId);
  };
})(jQuery, Drupal);
