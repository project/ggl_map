(function($, Drupal) {

  Drupal.AjaxCommands.prototype.updatePopupContent = function(ajax, response, status){
    let markerId = response.markerId;

    Drupal.gglMapMarkers.updatePopupContent(markerId, function() {

    });
  };
})(jQuery, Drupal);
