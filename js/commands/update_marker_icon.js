(function($, Drupal) {

  Drupal.AjaxCommands.prototype.updateMarkerIcon = function(ajax, response, status){
    let markerId = response.markerId;
    let url = response.url;
    let height = response.height;
    let width = response.width;
console.log(url);
    Drupal.gglMapMarkers.updateMarkerIcon(markerId, {
      'url': url,
      'height': height,
      'width': width
    });
  };
})(jQuery, Drupal);
