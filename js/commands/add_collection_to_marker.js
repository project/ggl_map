(function($, Drupal) {

  Drupal.AjaxCommands.prototype.addCollectionToMarker = function(ajax, response, status){
    let markerId = response.markerId;
    let collectionId = response.collectionId;

    let marker = Drupal.gglMapMarkers.allMarkers[markerId];

    Drupal.gglMapMarkers.addCollectionToMarker(collectionId, marker);
  };
})(jQuery, Drupal);
