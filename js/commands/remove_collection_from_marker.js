(function($, Drupal) {

  Drupal.AjaxCommands.prototype.removeCollectionFromMarker = function(ajax, response, status){
    let markerId = response.markerId;
    let collectionId = response.collectionId;

    let marker = Drupal.gglMapMarkers.allMarkers[markerId];

    Drupal.gglMapMarkers.removeCollectionFromMarker(collectionId, marker);
  };
})(jQuery, Drupal);
