(function($, Drupal) {

  Drupal.AjaxCommands.prototype.reloadCollectionByName = function(ajax, response, status){
    let name = response.name;

    Drupal.gglMapCollections.reloadCollectionByName(name);
  };
})(jQuery, Drupal);
