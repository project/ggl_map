/**
 * @file
 * Handle the ggl_maps.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.gglMap = Drupal.gglMap || {};
  Drupal.gglMap.updateOnDrag = false;
  Drupal.gglMap.attached = false;
  Drupal.gglMap.map = {};
  Drupal.gglMap.bounds = {};
  Drupal.gglMap.loaded = false;
  Drupal.gglMap.collectionsLoading = false;

    /**
   * Drupal behavior.
   * Initialize Station Locator only once.
   */
  Drupal.behaviors.gglMap = {
    attach: function (context, drupalSettings) {
      if (!Drupal.gglMap.attached) {
        // Make that this is executed once.
        Drupal.gglMap.loaded = false;
        Drupal.gglMap.attached = true;

        Drupal.gglMap.initGoogle(function () {
          // Replace google values by there constant value.
          Drupal.gglMapSettings.replaceGoogleConstants();

          //console.log(drupalSettings);
          // Get the party started.
          Drupal.gglMap.loadGglMap();
        });
      }
    }
  }

  /**
   * Get the party started.
   */
  Drupal.gglMap.loadGglMap = function () {
    let _self = this;
    _self.map = google.maps.Map;

    // Initialize the map.
    _self.initMap(function () {
      // Initialize the location search if enabled.
      if ($('#ggl_map_search_location').length > 0) {
        Drupal.gglMapLocation.initLocationSearch();
      }

      // Initialize the current location if enabled.
      if ($('#ggl_map_current_location').length > 0 ) {
        Drupal.gglMapLocation.initCurrentLocation();
      }

      // Load the collection(s) if none is loading at the moment.
      if (Drupal.gglMap.collectionsLoading === false) {
        Drupal.gglMapCollections.loadCollections(
          false,
          // All collections loaded callback.
          function (allSuccessful) {
            _self.loadingFinished(allSuccessful);
            Drupal.gglMapFilters.applyFilters('all', drupalSettings.gglMap.map.fitMapToMarkers);
          }
        );
      }

    });
  };

  Drupal.gglMap.initGoogle = function (callback) {

    // Check for google maps.
    if (typeof google === 'undefined' || typeof google.maps === 'undefined') {
      if (drupalSettings.gglMap.mapsApiLoading === true) {
        return;
      }

      drupalSettings.gglMap.mapsApiLoading = true;

      // Google maps isn't loaded so lazy load google maps.
      var scriptPath = '//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places';
      scriptPath += '&language=' + drupalSettings.gglMap.googleMapsApi.language;
      scriptPath += '&key=' + drupalSettings.gglMap.googleMapsApi.googleAPIkey;
      $.getScript(scriptPath).done(function () {
        drupalSettings.gglMap.mapsApiLoading = false;

        // Google maps loaded. Run callback.
        return callback();
      });

    }
    else {
      // Google maps already loaded. Run callback.
      return callback();
    }

  };

  Drupal.gglMap.initMap = function (callback) {
    let _self = this;

    if ($('#ggl_map').length > 0) {
      // The most important variable of all :)
      _self.map = new google.maps.Map(document.getElementById("ggl_map"), drupalSettings.gglMap.map);

      // Bind dragend event to the map.
      _self.map.addListener('dragend', function () {
        _self.boundsChanged();
      });

      // Bind zoom_changed event to the map.
      _self.map.addListener('zoom_changed', function () {
        _self.zoomChanged();
      });

      // Bind bounds_changed event to the map.
      _self.map.addListener('bounds_changed', function () {
        _self.bounds = _self.map.getBounds();
      });

      // Bind tilesloaded event to the map.
      _self.map.addListener('tilesloaded', function () {
        if (_self.loaded === false) {
          callback();
        }
      });

      // Bind click event to the map.
      _self.map.addListener('click', function () {
        if (typeof Drupal.gglMapMarkers.lastOpenMarker !== "undefined") {
          Drupal.gglMapMarkers.closePopup(Drupal.gglMapMarkers.lastOpenMarker);
        }
      });
    }
    else {
      if (drupalSettings.gglMap.project.debugging === true) {
        console.log('No element with id "ggl_map" found.');
      }
    }
  };

  /**
   * Zoom changed.
   */
  Drupal.gglMap.zoomChanged = function () {
    let _self = this;
    _self.boundsChanged();
  };

  /**
   * Bounds changed.
   */
  Drupal.gglMap.boundsChanged = function () {
    let _self = this;

    if (_self.timeout) {
      clearTimeout(_self.timeout);
    }

    _self.timeout = setTimeout(function () {
      // Load the collection(s) if none is loading at the moment.
      if (Drupal.gglMap.collectionsLoading === false) {
        Drupal.gglMapCollections.loadCollections(true, function(result) { });
      }
    }, 250);
  };

  /**
   * Get the four current map boundery coordinates.
   *
   * @param à
   * @returns {{swLat, neLng, neLat, swLng}}
   */
  Drupal.gglMap.getMapBounds = function () {
    let _self = this;

    let northEastLat = _self.bounds.getNorthEast().lat();
    let northEastLng = _self.bounds.getNorthEast().lng();
    let southWestLat = _self.bounds.getSouthWest().lat();
    let southWestLng = _self.bounds.getSouthWest().lng();

    return {"neLat": northEastLat, "neLng":  northEastLng, "swLat": southWestLat, "swLng":  southWestLng};
  };

  /**
   * Move to specific position.
   *
   * @param position
   * @param zoomLevel
   */
  Drupal.gglMap.moveTo = function (position, zoomLevel = 0) {

    Drupal.gglMap.map.panTo(position);

    // Zoom to it if zoomLevel is set.
    if (zoomLevel > 0) {
      Drupal.gglMap.map.setZoom(zoomLevel);
    }
  };

  /**
   * Fit map to markers.
   * @param markers
   * - Array of markers.
   * - OR Object with marker array per collection.
   */
  Drupal.gglMap.fitMapToMarkers = function (markers) {
    if (markers.length === 0 || drupalSettings.gglMap.map.fitMapToMarkers === false) {
      Drupal.gglMap.map.setCenter(drupalSettings.gglMap.map.center);
      return;
    }

    let bounds = new google.maps.LatLngBounds();
    markers.forEach(function (marker) {
      let position = marker.getPosition();
      if (position.lat() > 0 && position.lng() > 0) {
        bounds.extend(position);
      }
    });

    Drupal.gglMap.map.fitBounds(bounds);

    // Fitbounds 'callback'
    google.maps.event.addListenerOnce(Drupal.gglMap.map, 'idle', function () {
      Drupal.gglMap.refresh();
    });

  };

  /**
   * Loading finished.
   */
  Drupal.gglMap.loadingFinished = function (result) {
    Drupal.gglMap.loaded = true;

    // Trigger external JS after loading the map.
    $(window).trigger('gglMap:loadingFinished', {'result': result });
  };

  /**
   * Refresh the map.
   */
  Drupal.gglMap.refresh = function () {
    let _self = this;
    if(!_self.isEmpty(Drupal.gglMapMarkers.markerClusters)) {
      Drupal.gglMapMarkers.markerClusters.repaint();
    }
    google.maps.event.trigger(Drupal.gglMap.map, 'resize');
  };

  /**
   * Check if object is still empty {}.
   * @param object
   * @returns {boolean}
   */
  Drupal.gglMap.isEmpty = function (object) {
    for (const property in object) {
      return false;
    }
    return true;
  };

  /**
   * Convert an object to array.
   *
   * @returns {*[]}
   */
  Drupal.gglMap.objectToArray = function (object) {
    let array = [];
    let keys = Object.keys(object);
    keys.forEach((key) => {
      array.push(object[key]);
    });
    return array;
  };

})(jQuery, Drupal, drupalSettings);
