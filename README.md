# GGL map Quick Start

A GGL map can be integrated with the <code>ggl_map</code> theme.
<pre>
[
  "#theme" => "ggl_map",
  "#overrideSettings" => [                             <em>see below</em>
    {override default settings}
  ],
  "#collection" => [                                  <em>see below</em>
    {collection data},
    ...
  ],
]
</pre>
##\#overrideSettings</h4>

All settings provided in <a href="/admin/config/user-interface/ggl-map">/admin/config/user-interface/ggl-map</a>
can be overridden per GGL map instance.

An example of an <code>#overrideSettings</code> array:
<pre>
[
  "map": [
    "zoomControlOptions": [
      "position": "google.maps.ControlPosition.TOP_RIGHT"
    ],
  ],
  "location": [
    "enableSearchLocation": TRUE,
  ],
]
</pre>

##\#collection
The collection(s) must be provided in the themes <code>#collections</code> parameter.
In the array you can define one or more collections to be loaded on a map instance.
<pre>
[
  {
     "id": {identifier},
     "url": {references the marker data JSON},         <em>see below</em>
     "icon": {                                         <em>optional</em>
       "url": {references the marker icon}
       "height": {height}                              <em>optional</em>
       "width": {width}                                <em>optional</em>
     }
  },
  ...
]
</pre>

##Marker data JSON
 The <em>url</em> property in the themes #collections array references a json source with marker data:
 <pre>
 {
   "items": [
     {
       "id": {identifier},
       "lat": {lat},
       "lng": {lng},
       "icon": {                                         <em>optional</em>
         "url": {references the marker icon}
         "height": {height}                              <em>optional</em>
         "width": {width}                                <em>optional</em>
       },
       "popup": {
         "url": {references the content JSON}            <em>optional</em>
         "content": {html content}                       <em>optional</em>
       }
     },
     ...
   ]
 }
 </pre>

The markers are rendered on the map via <code>lat</code> and <code>lng</code> position and get an <code>icon</code>
according to the settings. It can be provided via the marker data JSON, else it is provided via the <em>marker</em> default settings.

>The <code>popup</code> element can have an <code>url</code> property referencing a JSON that holds the HTML content or
it provides the HTML content directly in the <code>content</code> property. That last option is not recommended when it comes to a lot of content per popup.

## Triggers
From the GGL map JS a lot of trigger are fired so that
project based JS logic can be executed. The names for the available triggers speak
for themselves.

* <code>gglMap:afterOpenPopup</code>
* <code>gglMap:afterUpdatePopup</code>
* <code>gglMap:startLoadingCollection</code>
* <code>gglMap:afterLoadingCollection</code>
* <code>gglMap:afterLoadingCollections</code>
* <code>gglMap:loadingFinished</code>

## AJAX commands

From the project interaction is possible with the VL map JS via AJAX commands.
The names for the available commands speak for themselves.

* <code>ClickMarkerCommand</code>
* <code>ReloadCollectionByNameCommand</code>
* <code>AddCollectionToMarkerCommand</code>
* <code>RemoveCollectionFromMarkerCommand</code>
* <code>UpdateMarkerIconCommand</code>
* <code>UpdatePopupContentCommands</code>




