(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * @namespace
   */
  Drupal.mapTrigger = Drupal.mapTrigger || {};

  /**
   * Drupal behavior.
   */
  Drupal.behaviors.mapTrigger = {
    attach: function (context, drupalSettings) {
      if (!Drupal.mapTrigger.attached) {
        Drupal.mapTrigger.attached = true;
        $(window).on({
          'gglMap:afterOpenPopup': function gglMapAfterOpenPopup(event, marker) {
            alert('popup for marker "' + marker.data.id + '" opened');
          },
          'gglMap:afterClosePopup': function gglMapAfterClosePopup(event, marker) {
            alert('popup for marker "' + marker.data.id + '" close');
          },
        });
      }
    }
  }

})(jQuery, Drupal, drupalSettings);
