<?php

namespace Drupal\ggl_map_examples;


class GglMapExamplesHelper {

  public function sideMenuItems() {
    return [
      [
        'title' => 'Single collection map',
        'uri' => base_path() . 'ggl_map_examples/single_map',
      ],
      [
        'title' => 'Single collection map with custom marker icon',
        'uri' => base_path() . 'ggl_map_examples/single_map_marker_icon',
      ],
      [
        'title' => 'Single collection map with location',
        'uri' => base_path() . 'ggl_map_examples/single_map_location',
      ],
      [
        'title' => 'Single collection map with #overrideSettings',
        'uri' => base_path() . 'ggl_map_examples/single_map_override_settings',
      ],
      [
        'title' => 'Multi collections map',
        'uri' => base_path() . 'ggl_map_examples/multi_map',
      ],
      [
        'title' => 'Multi collections map with clustering',
        'uri' => base_path() . 'ggl_map_examples/multi_map_cluster',
      ],
      [
        'title' => 'Map with JS triggers',
        'uri' => base_path() . 'ggl_map_examples/map_trigger',
      ],
      [
        'title' => 'Map with AJAX command',
        'uri' => base_path() . 'ggl_map_examples/map_ajax',
      ],
    ];
  }

}
