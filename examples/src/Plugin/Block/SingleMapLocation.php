<?php

namespace Drupal\ggl_map_examples\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "single_map_location_block",
 *  admin_label = @Translation("Single collection map with location"),
 * )
 */
class SingleMapLocation extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'single_map_location',
      '#ggl_map' => [
        "#theme" => "ggl_map",
        '#overrideSettings' => [
          'location' => [
            'enableSearchLocation' => TRUE,
            'enableCurrentLocation' => TRUE,
          ],
        ],
        '#collection' => [
          [
            'id' => 'single_map',
            'url' => '/modules/custom/ggl_map/examples/data/single_collection.json',
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'ggl_map_examples/map',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
