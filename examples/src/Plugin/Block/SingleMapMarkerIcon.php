<?php

namespace Drupal\ggl_map_examples\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "single_map_marker_icon_block",
 *  admin_label = @Translation("Single collection map with custom marker icon"),
 * )
 */
class SingleMapMarkerIcon extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'single_map_marker_icon',
      '#ggl_map' => [
        "#theme" => "ggl_map",
        '#collection' => [
          [
            'id' => 'single_map',
            'url' => '/modules/custom/ggl_map/examples/data/single_collection_marker_icon.json',
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'ggl_map_examples/map',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
