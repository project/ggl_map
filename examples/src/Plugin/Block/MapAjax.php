<?php

namespace Drupal\ggl_map_examples\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "map_ajax_block",
 *  admin_label = @Translation("Map with AJAX command"),
 * )
 */
class MapAjax extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'map_ajax',
      '#ggl_map' => [
        "#theme" => "ggl_map",
        '#collection' => [
          [
            'id' => 'single_map',
            'url' => '/modules/custom/ggl_map/examples/data/single_collection.json',
          ],
        ],
      ],
      '#switchMarker1ToRed' => $this->switchToRed('marker_1'),
      '#switchMarker2ToRed' => $this->switchToRed('marker_2'),
      '#switchMarker3ToRed' => $this->switchToRed('marker_3'),
      '#attached' => [
        'library' => [
          'ggl_map_examples/map',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * @return Link
   */
  private function switchToRed($markerId) {
    $url = Url::fromRoute('ggl_map_examples.map_ajax_switch_to_red', ['markerId' => $markerId]);
    $url->setOptions([
      'attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
    ]);
    return new Link('Switch "' . $markerId. '" to red', $url);
  }

}
