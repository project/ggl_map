<?php

namespace Drupal\ggl_map_examples\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "multi_map_block",
 *  admin_label = @Translation("Multi map"),
 * )
 */
class MultiMap extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'multi_map',
      '#ggl_map' => [
        "#theme" => "ggl_map",
        '#collection' => [
          [
            'id' => 'multi_map_1',
            'url' => '/modules/custom/ggl_map/examples/data/multi_collection_1.json',
            'icon' => [
              'url' => '/modules/custom/ggl_map/examples/images/marker_red.svg',
            ],
          ],
          [
            'id' => 'multi_map_2',
            'url' => '/modules/custom/ggl_map/examples/data/multi_collection_2.json',
            'icon' => [
              'url' => '/modules/custom/ggl_map/examples/images/marker_blue.svg',
            ],
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'ggl_map_examples/map',
        ],
      ],
    ];
  }

}
