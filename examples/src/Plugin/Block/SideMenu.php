<?php

namespace Drupal\ggl_map_examples\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ggl_map_examples\GglMapExamplesHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "side_menu_block",
 *  admin_label = @Translation("Side menu"),
 * )
 */
class SideMenu extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\ggl_map_examples\GglMapExamplesHelper|object|null
   */
  private GglMapExamplesHelper $gglMapExamplesHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $gglMapExamplesHelper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->gglMapExamplesHelper = $gglMapExamplesHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('ggl_map_examples.ggl_map_examples_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'side_menu',
      '#items' => $this->gglMapExamplesHelper->sideMenuItems(),
      '#requestUri' => \Drupal::request()->getRequestUri(),
    ];
  }

}
