<?php

namespace Drupal\ggl_map_examples\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Block\BlockManager;
use Drupal\ggl_map\Ajax\UpdateMarkerIconCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class GglMapExamplesController extends ControllerBase {
  /**
   * @var \Drupal\Core\Block\BlockManager
   */
  private BlockManager $blockManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(BlockManager $blockManager) {
    $this->blockManager = $blockManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
    );
  }

  /**
   *
   */
  public function singleMap() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('single_map_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   *
   */
  public function singleMapMarkerIcon() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('single_map_marker_icon_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   *
   */
  public function singleMapOverrideSettings() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('single_map_override_settings_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   *
   */
  public function singleMapLocation() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('single_map_location_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   *
   */
  public function multiMap() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('multi_map_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   *
   */
  public function multiMapCluster() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('multi_map_cluster_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   *
   */
  public function mapTrigger() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('map_trigger_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   *
   */
  public function mapAjax() {
    return [
      '#theme' => 'example_page',
      '#side_menu' => $this->blockManager->createInstance('side_menu_block')->build(),
      '#content' => $this->blockManager->createInstance('map_ajax_block')->build(),
      '#attached' => [
        'library' => [
          'ggl_map_examples/page',
        ],
      ],
    ];
  }

  /**
   * @return AjaxResponse
   */
  public function mapAjaxSwitchToRed($markerId) {
    $response = new AjaxResponse();
    $response->addCommand(new UpdateMarkerIconCommand($markerId, '/modules/custom/ggl_map/examples/images/marker_red.svg'));

    return $response;
  }

}
