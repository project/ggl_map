<?php

namespace Drupal\ggl_map\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 *
 */
class RemoveCollectionFromMarkerCommand implements CommandInterface {

  /**
   * @var string
   */
  private string $collectionId;

  /**
   * @var string
   */
  private string $markerId;

  /**
   *
   */
  public function __construct(string $collectionId, string $markerId) {
    $this->markerId = $markerId;
    $this->collectionId = $collectionId;
  }

  /**
   *
   */
  public function render(): array {
    return [
      'command' => 'removeCollectionFromMarker',
      'collectionId' => $this->collectionId,
      'markerId' => $this->markerId,
    ];
  }

}
