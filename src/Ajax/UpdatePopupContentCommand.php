<?php

namespace Drupal\ggl_map\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 *
 */
class UpdatePopupContentCommand implements CommandInterface {

  /**
   * @var string
   */
  private string $markerId;

  /**
   *
   */
  public function __construct(string $markerId) {
    $this->markerId = $markerId;
  }

  /**
   *
   */
  public function render(): array {
    return [
      'command' => 'updatePopupContent',
      'markerId' => $this->markerId,
    ];
  }

}
