<?php

namespace Drupal\ggl_map\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 *
 */
class ClickMarkerCommand implements CommandInterface {

  /**
   * @var string
   */
  private string $markerId;

  /**
   *
   */
  public function __construct(string $markerId) {
    $this->markerId = $markerId;
  }

  /**
   * @return array
   */
  public function render(): array {
    return [
      'command' => 'clickMarker',
      'markerId' => $this->markerId,
    ];
  }

}
