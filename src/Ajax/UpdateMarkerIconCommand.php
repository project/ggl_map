<?php

namespace Drupal\ggl_map\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 *
 */
class UpdateMarkerIconCommand implements CommandInterface {

  /**
   * @var string
   */
  private string $url;

  /**
   * @var string
   */
  private string $markerId;

  /**
   *
   */
  public function __construct(string $markerId, string $url) {
    $this->markerId = $markerId;
    $this->url = $url;
  }

  /**
   *
   */
  public function render(): array {
    return [
      'command' => 'updateMarkerIcon',
      'markerId' => $this->markerId,
      'url' => $this->url,
    ];
  }

}
