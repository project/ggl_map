<?php

namespace Drupal\ggl_map\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 *
 */
class ReloadCollectionByNameCommand implements CommandInterface {

  /**
   * @var string
   */
  private string $name;

  /**
   *
   */
  public function __construct(string $name) {
    $this->name = $name;
  }

  /**
   *
   */
  public function render(): array {
    return [
      'command' => 'reloadCollectionByName',
      'name' => $this->name,
    ];
  }

}
