<?php

namespace Drupal\ggl_map\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * The admin settings form for GGL Map.
 */
class GglMapSettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  const YAML_ELEMENTS = [
    'google_maps_api',
    'project',
    'map',
    'marker',
    'cluster',
    'collection',
    'location'
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->config = $this->config('ggl_map.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'ggl_map_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['ggl_map.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['settings_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Default settings'),
    ];

    $form['project_tab'] = [
      '#type' => 'details',
      '#title' => t('Project'),
      '#group' => 'settings_tabs',
    ];

    $form['project_tab']['project'] = [
      '#type' => 'textarea',
      '#default_value' => $this->valueToYaml('project'),
      '#attributes' => ['data-yaml-editor' => 'true'],
    ];

    $form['google_maps_api_tab'] = [
      '#type' => 'details',
      '#title' => t('Google maps API'),
      '#group' => 'settings_tabs',
    ];

    $form['google_maps_api_tab']['google_maps_api'] = [
      '#type' => 'textarea',
      '#default_value' => $this->valueToYaml('google_maps_api'),
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#description' => $this->t('A <em>googleAPIkey</em> can be obtained via
        <a target="_blank" href="https://developers.google.com/maps/get-started">developers.google.com/maps</a>.<br>
        Make sure:
        <ul>
          <li>you activate the <em>Maps Javascript API</em> and the <em>Places API</em></li>
          <li>you restrict the key usage to only your project URL to prevent abuse</li>
        </ul>')
    ];


    $form['map_tab'] = [
      '#type' => 'details',
      '#title' => t('Map'),
      '#group' => 'settings_tabs',
    ];

    $form['map_tab']['map'] = [
      '#type' => 'textarea',
      '#default_value' => $this->valueToYaml('map'),
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#description' => $this->t('<ul>
        <li>
        All map settings described on <a target="_blank" href="https://developers.google.com/maps/documentation/javascript/overview">developers.google.com/maps/documentation/javascript/overview</a>
        come here.
        </li>
         <li>
        The <em>styles</em> property contains the styling of the map.
        This can be done with tools like e.g. <a target="_blank" href="https://snazzymaps.com">snazzymaps.com</a>.
        The JSON data it usually delivers can be converted to YAML with a tool like e.g.
        <a target="_blank" href="https://www.json2yaml.com">json2yaml.com</a>.
        </li>
      </ul>'),
    ];

    $form['collection_tab'] = [
      '#type' => 'details',
      '#title' => t('Collection'),
      '#group' => 'settings_tabs',
    ];

    $form['collection_tab']['collection'] = [
      '#type' => 'textarea',
      '#default_value' => $this->valueToYaml('collection'),
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#description' => '<ul>
         <li><em>limitToBounds</em> <code>true</code> adds a <code>/[swLat]--[neLat]/[swLng]--[neLng]</code> to the collection url
(which represent the map bounds) and can be picked up in the backend to reduce the collection data. The collection will be
reloaded on a map bounds change.</li>
         <li><em>toggleOnLoad</em> <code>true</code> will make the markers visible on map load. If <code>false</code> the
         collection can be toggled via JS. Check out <code>Drupal.gglMapToggles.toggleCollection()</code>.</li>
         </ul>',
    ];

    $form['marker_tab'] = [
      '#type' => 'details',
      '#title' => t('Marker'),
      '#group' => 'settings_tabs',
    ];

    $form['marker_tab']['marker'] = [
      '#type' => 'textarea',
      '#default_value' => $this->valueToYaml('marker'),
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#description' => 'If no icon is provided within the collection data, this default <em>icon</em> will be used.'
    ];

    $form['cluster_tab'] = [
      '#type' => 'details',
      '#title' => t('Cluster'),
      '#group' => 'settings_tabs',
    ];

    $form['cluster_tab']['cluster'] = [
      '#type' => 'textarea',
      '#default_value' => $this->valueToYaml('cluster'),
      '#attributes' => ['data-yaml-editor' => 'true'],
    ];

    $form['location_tab'] = [
      '#type' => 'details',
      '#title' => t('Location'),
      '#group' => 'settings_tabs',
    ];

    $form['location_tab']['location'] = [
      '#type' => 'textarea',
      '#default_value' => $this->valueToYaml('location'),
      '#attributes' => ['data-yaml-editor' => 'true'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    foreach (self::YAML_ELEMENTS as $element) {
      try {
        Yaml::parse($form_state->getValue($element));
      } catch (\Exception $e) {
        $form_state->setError($form[$element . '_tab'][$element], $this->t('Invalid YAML value in %element', ['%element' => $element]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config
      ->set('google_maps_api', $this->yamlToValue($form_state, 'google_maps_api'))
      ->set('project', $this->yamlToValue($form_state, 'project'))
      ->set('map', $this->yamlToValue($form_state, 'map'))
      ->set('marker', $this->yamlToValue($form_state, 'marker'))
      ->set('collection', $this->yamlToValue($form_state, 'collection'))
      ->set('location', $this->yamlToValue($form_state, 'location'))
      ->set('cluster', $this->yamlToValue($form_state, 'cluster'))
      ->save();

    parent::submitForm($form, $form_state);

    // Make sure the ggl_maps's adopt the updated default settings.
    Cache::invalidateTags(['ggl_map']);
  }

  /**
   * Get the YAML value.
   *
   * @param $element
   *
   * @return string
   */
  private function valueToYaml($element) {
    $value = $this->config->get($element);
    if ($value == NULL) {
      return '';
    }
    return Yaml::dump($value);
  }

  /**
   * Get the value.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param $element
   *
   * @return mixed|void
   */
  private function yamlToValue(FormStateInterface $formState, $element) {
    try {
      return Yaml::parse($formState->getValue($element));
    }
    catch (\Exception $e) {
    }
  }

}
