<?php

namespace Drupal\ggl_map\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ggl_map_markers_field' widget.
 *
 * @FieldWidget(
 *     id="ggl_map_markers_widget",
 *     module="ggl_map",
 *     label=@Translation("GGL Map Markers Widget"),
 *     field_types={
 *         "ggl_map_markers_field"
 *     }
 * )
 */
class GglMapMarkersWidget extends WidgetBase {

  /**
   * formElement().
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param int $delta
   * @param array $element
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = [
      "name" => [
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#default_value' => $items[$delta]->name,
      ],
      "lat" => [
        '#type' => 'textfield',
        '#title' => t('Lat'),
        '#default_value' => $items[$delta]->lat,
      ],
      "lng" => [
        '#type' => 'textfield',
        '#title' => t('Lng'),
        '#default_value' => $items[$delta]->lng,
      ],
      "icon_url" => [
        '#type' => 'textfield',
        '#title' => t('Icon URL'),
        '#default_value' => $items[$delta]->icon_url,
      ],
      "icon_height" => [
        '#type' => 'textfield',
        '#title' => t('Icon height'),
        '#default_value' => $items[$delta]->icon_height,
      ],
      "icon_width" => [
        '#type' => 'textfield',
        '#title' => t('Icon width'),
        '#default_value' => $items[$delta]->icon_width,
      ],
      "content" => [
        '#type' => 'textarea',
        '#title' => t('Popup content'),
        '#default_value' => $items[$delta]->content,
      ],
    ];

    if ($this->getFieldSetting('icon_url_via_template') == 1) {
      unset($element['value']['icon_url']);
    }

    if ($this->getFieldSetting('icon_height_via_template') == 1 || $this->getFieldSetting('icon_height_via_template') == NULL) {
      unset($element['value']['icon_height']);
    }

    if ($this->getFieldSetting('icon_width_via_template') == 1 || $this->getFieldSetting('icon_width_via_template') == NULL) {
      unset($element['value']['icon_width']);
    }

    return $element;
  }

  /**
   * massageFormValues.
   *
   * @param array $values
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $_values = [];
    foreach ($values as $delta => $value) {
      $value['value']['lat'] = str_replace(',', '.', $value['value']['lat']);
      $value['value']['lng'] = str_replace(',', '.', $value['value']['lng']);
      $_values[] = $value['value'];
    }
    return $_values;
  }

}
