<?php

namespace Drupal\ggl_map\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'GGL Map Markers Field' formatter.
 *
 * @FieldFormatter(
 *   id = "ggl_map_markers_formatter",
 *   label = @Translation("GGL Map Markers"),
 *   field_types = {
 *     "ggl_map_markers_field"
 *   }
 * )
 */
class GglMapMarkersFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = ["#type" => "markup", "#markup" => $item->name];
    }

    return $element;
  }

}
