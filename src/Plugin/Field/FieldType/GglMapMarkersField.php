<?php

namespace Drupal\ggl_map\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'Template Field' field type.
 *
 * @FieldType (
 *   id = "ggl_map_markers_field",
 *   label = @Translation("GGL Map Markers"),
 *   description = @Translation("Stores the data of GGL Map Markers"),
 *   default_widget = "ggl_map_markers_widget",
 *   default_formatter = "ggl_map_markers_formatter",
 * )
 */
class GglMapMarkersField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'content' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
        'lat' => [
          'not null' => FALSE,
          'type' => 'float',
          'size' => 'big',
          'default' => 0,
        ],
        'lng' => [
          'not null' => FALSE,
          'type' => 'float',
          'size' => 'big',
          'default' => 0,
        ],
        'icon_url' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'icon_height' => [
          'not null' => FALSE,
          'type' => 'int',
          'size' => 'normal',
          'default' => 0,
        ],
        'icon_width' => [
          'not null' => FALSE,
          'type' => 'int',
          'size' => 'normal',
          'default' => 0,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value1 = $this->get('name')->getValue();
    $value2 = $this->get('content')->getValue();
    $value3 = $this->get('lat')->getValue();
    $value4 = $this->get('lng')->getValue();
    $value5 = $this->get('icon_url')->getValue();
    $value6 = $this->get('icon_height')->getValue();
    $value7 = $this->get('icon_width')->getValue();
    return empty($value1) && empty($value2) && empty($value3) && empty($value4) && empty($value5) && empty($value6) && empty($value7);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    // Add our properties.
    $properties['name'] = DataDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Name'));

    $properties['lat'] = DataDefinition::create('string')
      ->setLabel(t('Latitude'))
      ->setDescription(t('Latitude'));

    $properties['lng'] = DataDefinition::create('string')
      ->setLabel(t('Longitude'))
      ->setDescription(t('Longitude'));

    $properties['content'] = DataDefinition::create('string')
      ->setLabel(t('Popup Content'))
      ->setDescription(t('Popup Content'));

    $properties['icon_url'] = DataDefinition::create('string')
      ->setLabel(t('Icon URL'))
      ->setDescription(t('Icon URL'));

    $properties['icon_height'] = DataDefinition::create('string')
      ->setLabel(t('Icon Height'))
      ->setDescription(t('Icon Height'));

    $properties['icon_width'] = DataDefinition::create('string')
      ->setLabel(t('Icon Width'))
      ->setDescription(t('Icon Width'));

    return $properties;
  }

  /**
   * @return array|bool[]
   */
  public static function defaultFieldSettings() {
    return [
        'icon_url_via_template' => 1,
        'icon_height_via_template' => 1,
        'icon_width_via_template' => 1,
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element = [];
    // The key of the element should be the setting name
    $element['icon_url_via_template'] = [
      '#title' => $this->t('Icon URL via template'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('icon_url_via_template') ?? 1,
    ];

    $element['icon_height_via_template'] = [
      '#title' => $this->t('Icon Height via template'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('icon_height_via_template') ?? 1,
    ];

    $element['icon_width_via_template'] = [
      '#title' => $this->t('Icon Width via template'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('icon_width_via_template') ?? 1,
    ];

    return $element;
  }

}
