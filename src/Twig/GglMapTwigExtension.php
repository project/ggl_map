<?php

namespace Drupal\ggl_map\Twig;

use Drupal\Core\Render\Renderer;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class DefaultService.
 *
 * @package Drupal\ggl_map
 */
class GglMapTwigExtension extends AbstractExtension {

  /**
   * @var \Drupal\Core\Render\Renderer
   */
  private Renderer $renderer;

  /**
   * @param Renderer $renderer
   */
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * @return \Twig\TwigFunction[]
   */
  public function getFunctions() {
    return [
      new TwigFunction('gglMap', [$this, 'gglMap'], ['is_safe' => ['html']]),
    ];
  }

  /**
   * The method gglMap itself.
   *
   * @param $string
   *
   * @return string
   */
  public function gglMap(array $collection, array $overrideSettings = []) {
    $map = [
      "#theme" => "ggl_map",
      '#overrideSettings' => $overrideSettings,
      '#collection' => [$collection],
    ];

    return $this->renderer->render($map);
  }

}
